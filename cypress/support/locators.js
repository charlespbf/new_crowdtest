const locators = {
  LOGIN: {
    USER: '#login',
    PASSWORD: '#password',
    BTN_LOGIN: ':nth-child(3) > .btn'
  },
  MESSAGES: {
    WELCOME: '.title > .col > span',
    LOGIN_ERRO: '.login-error',
    EMAIL_ERRO: '#elementErrorMsg_login',
    PASSWORD_ERRO: '#elementErrorMsg_password',
    PROJETO_SALVO: '.alert-box > p',
    FECHAR_ALERT_BOX: '.alert-box-actions > .btn',
    INFORME_DESC_PROJETO: '#elementErrorMsg_description'
  },
  HOME: {
    GERENCIAR_PROJETOS: '.manager > .action > .col > .btn'
  },
  MENU_PROJETOS: {
    PROJETOS: '.li-projects > a > .crowd-icon',
    NOVO_PROJETO: '.btn',
    ID_PROJETO: '#identifier',
    NOME_PROJETO: '#name',
    DESCRICAO_PROJETO: '#description',
    ADMINS_PROJETO: '#admins',
    TIPO_PROJETO: '.ng-select-container',
    FN_XP_PROJETO_WEB:()=> `//span[contains(., 'Web')]`,    
    LINK_APLICACAO: '#link',
    BTN_SALVAR_PROJETO:'.btn-crowdtest',
    EDITAR_PROJETO: '.cdk-column-actions > :nth-child(1) > .ng-fa-icon > .svg-inline--fa > path',
    CONFIRMA_EDIT_PROJETO: '.btn-crowdtest > .ng-star-inserted',
    NOME_EDIT_PROJETO: ':nth-child(2) > .col > p',
    EXCLUIR_PROJETO: '.cdk-column-actions > :nth-child(2) > .ng-fa-icon > .svg-inline--fa > path',
    CONFIRMA_EXCLUIR_PROJETO: '.btn-crowdtest > .ng-star-inserted'

  },
}

export default locators;