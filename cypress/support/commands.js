// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import loc from './locators'

Cypress.Commands.add('login', function() {
  cy.visit('http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com')
    cy.fixture('userData').as('usuario').then(() => {
       cy.get(loc.LOGIN.USER).type(this.usuario.email)
       cy.get(loc.LOGIN.PASSWORD).type(this.usuario.password)
    })
  cy.get(loc.LOGIN.BTN_LOGIN).click()
  cy.get(loc.MESSAGES.WELCOME).should('contain', 'Bem-vindo ao Crowdtest! O que deseja fazer hoje?')
})