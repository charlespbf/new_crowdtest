/// <reference types="cypress" />

import loc from '../support/locators'

describe('Fluxo de Exceção no Login', () => {

  beforeEach(() => {
    cy.visit('http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com')
  })

  it('E-mail ou senha inválidos', function() {
    cy.fixture('userData').as('usuario').then(() => {
      cy.get(loc.LOGIN.USER).type(this.usuario.emailFake)
      cy.get(loc.LOGIN.PASSWORD).type(this.usuario.passwordFake)
    })
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.get(loc.MESSAGES.LOGIN_ERRO).should('contain', 'E-mail ou senha inválidos.')
  })

  it('E-mail Inválido', function() {
    cy.fixture('userData').as('usuario').then(() => {
      cy.get(loc.LOGIN.USER).type(this.usuario.emailInvalido)
      cy.get(loc.LOGIN.PASSWORD).type(this.usuario.passwordFake)
    })
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.get(loc.MESSAGES.EMAIL_ERRO).should('contain', 'EMAIL inválido')
  })

  it('E-mail não informado', function() {
    cy.fixture('userData').as('usuario').then(() => {
      cy.get(loc.LOGIN.PASSWORD).type(this.usuario.password)
    })
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.get(loc.MESSAGES.EMAIL_ERRO).should('contain', 'Informe o e-mail')
  })

  it('Senha não informada', function() {
    cy.fixture('userData').as('usuario').then(() => {
      cy.get(loc.LOGIN.USER).type(this.usuario.email)
    })
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.get(loc.MESSAGES.PASSWORD_ERRO).should('contain', 'Informe a senha')
  })
})

