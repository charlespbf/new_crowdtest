/// <reference types="cypress" />

import loc from '../support/locators'

describe('Fluxo Exceção Projetos', () => {
  beforeEach(() => {
    cy.login()
  })

  it('Criar novo Projeto', () => {
    cy.get(loc.HOME.GERENCIAR_PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.NOVO_PROJETO).click()
    cy.get(loc.MENU_PROJETOS.ID_PROJETO).type ('01')
    cy.get(loc.MENU_PROJETOS.NOME_PROJETO).type ('Projeto Cypress')
    cy.get(loc.MENU_PROJETOS.DESCRICAO_PROJETO).type ('Projeto de teste criado via Cypress')
    cy.get(loc.MENU_PROJETOS.ADMINS_PROJETO).type ('Bené, Pedro de Lara')
    cy.get(loc.MENU_PROJETOS.TIPO_PROJETO).click()
    cy.xpath(loc.MENU_PROJETOS.FN_XP_PROJETO_WEB('Web')).click()
    cy.get(loc.MENU_PROJETOS.LINK_APLICACAO).type('http://linkdaaplicacao.com')
    cy.get(loc.MENU_PROJETOS.BTN_SALVAR_PROJETO).click()
    cy.wait(5000)
    cy.get(loc.MESSAGES.PROJETO_SALVO).should('contain', 'Projeto criado com sucesso.')
    cy.get(loc.MESSAGES.FECHAR_ALERT_BOX).click()
  })

  it('Criar Projeto com ID existente', () => {
    cy.get(loc.HOME.GERENCIAR_PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.NOVO_PROJETO).click()
    cy.get(loc.MENU_PROJETOS.ID_PROJETO).type ('01')
    cy.get(loc.MENU_PROJETOS.NOME_PROJETO).type ('Projeto Cypress')
    cy.get(loc.MENU_PROJETOS.DESCRICAO_PROJETO).type ('Projeto de teste criado via Cypress')
    cy.get(loc.MENU_PROJETOS.ADMINS_PROJETO).type ('Bené, Pedro de Lara')
    cy.get(loc.MENU_PROJETOS.TIPO_PROJETO).click()
    cy.xpath(loc.MENU_PROJETOS.FN_XP_PROJETO_WEB('Web')).click()
    cy.get(loc.MENU_PROJETOS.LINK_APLICACAO).type('http://linkdaaplicacao.com')
    cy.get(loc.MENU_PROJETOS.BTN_SALVAR_PROJETO).click()
    cy.get(loc.MESSAGES.PROJETO_SALVO).should('contain', 'ID já foi utilizado.')
    cy.get(loc.MESSAGES.FECHAR_ALERT_BOX).click()
  })

  it('Incluir Projeto sem informar campo obrigatório', () => {
    cy.get(loc.HOME.GERENCIAR_PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.NOVO_PROJETO).click()
    cy.get(loc.MENU_PROJETOS.ID_PROJETO).type ('01')
    cy.get(loc.MENU_PROJETOS.NOME_PROJETO).type ('Projeto Cypress')
    cy.get(loc.MENU_PROJETOS.ADMINS_PROJETO).type ('Bené, Pedro de Lara')
    cy.get(loc.MENU_PROJETOS.TIPO_PROJETO).click()
    cy.xpath(loc.MENU_PROJETOS.FN_XP_PROJETO_WEB('Web')).click()
    cy.get(loc.MENU_PROJETOS.LINK_APLICACAO).type('http://linkdaaplicacao.com')
    cy.get(loc.MENU_PROJETOS.BTN_SALVAR_PROJETO).click()
    cy.get(loc.MESSAGES.INFORME_DESC_PROJETO).should('contain', 'Informe a descrição do projeto.')
    
  })
  it('Excluir Projeto', () => {
    cy.get(loc.HOME.GERENCIAR_PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.PROJETOS).click()
    cy.get(loc.MENU_PROJETOS.EXCLUIR_PROJETO).click()
    cy.get(loc.MENU_PROJETOS.CONFIRMA_EXCLUIR_PROJETO).click()
    cy.get(loc.MESSAGES.PROJETO_SALVO).should('contain', 'Projeto apagado com sucesso.')
    cy.get(loc.MESSAGES.FECHAR_ALERT_BOX).click()

   })
 
})